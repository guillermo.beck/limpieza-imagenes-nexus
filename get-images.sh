#!/bin/bash

nexus-cli image ls | \
  while read image
  do
    nexus-cli image tags --name $image | \
        while read tag 
        do
            echo "nexus-cli image delete --name $image --tag $tag" >> nexus-images-delete.log
        done
  done
