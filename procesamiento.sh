#!/bin/bash


#solo es necesario. f_nexus_images.txt y f_ocp_images.txt

cat nexus-images-delete.log | grep -v There  | sort | uniq > f_nexus_images.txt

cat ocp-* | grep  -v 'tag $' | sort | uniq  > f_ocp_images.txt

cat f_ocp_images.txt | awk '{print $1}' |\
    while read image_ocp 
    do
        #echo $image_ocp
        grep $image_ocp f_nexus_images.txt >> f_nexus_borrar-tmp.txt
    done

    cat f_nexus_borrar-tmp.txt | sort | uniq > f_nexus_borrar-v1.txt #incluidas todas. se debe refinar para sacar las que si se usan. a partir de estas se deben ver cuales se borrar y cuales se mantienen.
    rm f_nexus_borrar-tmp.txt
    cp f_nexus_borrar-v1.txt f_nexus_borrar-v2.txt
    
cat f_ocp_images.txt | \
    while read nexus_image 
    do
        #echo "sed -i /$nexus_image/d f_nexus_borrar-v2.txt"
        sed -i "/$nexus_image/d" f_nexus_borrar-v2.txt
    done